> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Header 1

## Header 2

Now is the time for all good men to come to
the aid of their country. This is just a
regular paragraph.

The quick brown fox jumped over the lazy
dog's back.

### Header 3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> ## This is an H2 in a blockquote

1. Mobile Application Development and Management Fall 2015 a1
2. Alexander Garcia
3. Assignment requirements
4. Git commands and short description
5. Link to your Web site

Here's an exmple link: [example link](http://example.com/ "With a Title")
